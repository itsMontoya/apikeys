package apikeys

import (
	"time"

	"github.com/Hatch1fy/errors"
)

func newAPIKey(userID, label string) (a APIKey) {
	a.UserID = userID
	a.CreatedTS = time.Now().Unix()
	a.Label = label
	return
}

// APIKey represents an api key reference
type APIKey struct {
	UserID    string `json:"userID"`
	CreatedTS int64  `json:"createdTS"`
	UsedTS    int64  `json:"userTS"`
	Label     string `json:""`
}

// Validate will validate an API key
func (a *APIKey) Validate() (err error) {
	var errs errors.ErrorList
	if len(a.UserID) == 0 {
		errs.Push(ErrInvalidUserID)
	}

	if len(a.Label) == 0 {
		errs.Push(ErrInvalidLabel)
	}

	return errs.Err()
}
