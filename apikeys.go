package apikeys

import (
	"encoding/json"
	"path"

	"github.com/Hatch1fy/errors"
	"github.com/boltdb/bolt"
	"github.com/missionMeteora/uuid"
)

var (
	// apiKeysBktKey is the key for the apiKeys bucket
	apiKeysBktKey = []byte("apiKeys")
	// ownersBktKey is the key for the apiKeys bucket
	ownersBktKey = []byte("owners")
)

const (
	// ErrNotInitialized is returned when actions are performed on a non-initialized instance of APIKeys
	ErrNotInitialized = errors.Error("apiKeys library has not been properly initialized")
	// ErrAPIKeyNotFound is returned when a api key is not found
	ErrAPIKeyNotFound = errors.Error("api key not found")
	// ErrInvalidUserID is returned when a user id is empty
	ErrInvalidUserID = errors.Error("invalid user id, cannot be empty")
	// ErrInvalidLabel is returned when a APIKey's label is empty
	ErrInvalidLabel = errors.Error("invalid label, cannot be empty")
)

// New will return a new instance of APIKeys
func New(dir string) (ap *APIKeys, err error) {
	var a APIKeys
	if a.db, err = bolt.Open(path.Join(dir, "apiKeys.bdb"), 0644, nil); err != nil {
		return
	}

	// Initialize database and db utilities
	if err = a.init(); err != nil {
		return
	}

	// Create UUID generator
	a.gen = uuid.NewGen()

	// Assign pointer to created instance of APIKeys
	ap = &a
	return
}

// APIKeys manages the apiKeys service
type APIKeys struct {
	db  *bolt.DB
	gen *uuid.Gen
}

func (a *APIKeys) init() (err error) {
	err = a.db.Update(func(txn *bolt.Tx) (err error) {
		if _, err = txn.CreateBucketIfNotExists(apiKeysBktKey); err != nil {
			return
		}

		_, err = txn.CreateBucketIfNotExists(ownersBktKey)
		return
	})

	return
}

func (a *APIKeys) get(txn *bolt.Tx, id []byte) (ap *APIKey, err error) {
	bkt := txn.Bucket(apiKeysBktKey)

	var jsonBytes []byte
	if jsonBytes = bkt.Get(id); len(jsonBytes) == 0 {
		err = ErrAPIKeyNotFound
		return
	}

	var apiKey APIKey
	if err = json.Unmarshal(jsonBytes, &apiKey); err != nil {
		return
	}

	ap = &apiKey
	return
}

func (a *APIKeys) getSlice(txn *bolt.Tx, ids [][]byte) (apiKeys []*APIKey, err error) {
	// Pre-allocate apiKeys slice
	apiKeys = make([]*APIKey, 0, len(ids))

	for _, id := range ids {
		var apiKey *APIKey
		if apiKey, err = a.get(txn, []byte(id)); err != nil {
			return
		}

		apiKeys = append(apiKeys, apiKey)
	}

	return
}

func (a *APIKeys) getOwnedIDs(txn *bolt.Tx, ownerID []byte) (ids [][]byte, err error) {
	var ownersBkt *bolt.Bucket
	if ownersBkt = txn.Bucket(ownersBktKey); ownersBkt == nil {
		return nil, ErrNotInitialized
	}

	var ownerBkt *bolt.Bucket
	if ownerBkt = ownersBkt.Bucket([]byte(ownerID)); ownerBkt == nil {
		return
	}

	err = ownerBkt.ForEach(func(idBytes, _ []byte) (err error) {
		ids = append(ids, idBytes)
		return
	})

	return
}

// put will set a apiKey
func (a *APIKeys) put(txn *bolt.Tx, id []byte, apiKey *APIKey) (err error) {
	var bkt *bolt.Bucket
	if bkt = txn.Bucket(apiKeysBktKey); bkt == nil {
		return ErrNotInitialized
	}

	var jsonBytes []byte
	if jsonBytes, err = json.Marshal(apiKey); err != nil {
		return
	}

	return bkt.Put(id, jsonBytes)
}

func (a *APIKeys) assignOwner(txn *bolt.Tx, ownerID, id []byte) (err error) {
	var ownersBkt *bolt.Bucket
	if ownersBkt = txn.Bucket(ownersBktKey); ownersBkt == nil {
		return ErrNotInitialized
	}

	var ownerBkt *bolt.Bucket
	if ownerBkt, err = ownersBkt.CreateBucketIfNotExists([]byte(ownerID)); err != nil {
		return
	}

	return ownerBkt.Put(id, nil)
}

// delete will remove a apiKey
func (a *APIKeys) delete(txn *bolt.Tx, id []byte) (err error) {
	var bkt *bolt.Bucket
	if bkt = txn.Bucket(apiKeysBktKey); bkt == nil {
		return ErrNotInitialized
	}

	return bkt.Delete(id)
}

// New will create a new apiKey and return the associated ID
func (a *APIKeys) New(userID, label string) (key string, err error) {
	apiKey := newAPIKey(userID, label)
	if err = apiKey.Validate(); err != nil {
		return
	}

	// Generate API key
	key = a.gen.New().String()
	// Get byteslice version of API key string
	keyBytes := []byte(key)

	err = a.db.Update(func(txn *bolt.Tx) (err error) {
		if err = a.assignOwner(txn, []byte(userID), keyBytes); err != nil {
			return
		}

		if err = a.put(txn, keyBytes, &apiKey); err != nil {
			return
		}

		return
	})

	return
}

// Get will return the APIKey associated with the provided id
func (a *APIKeys) Get(id string) (apiKey *APIKey, err error) {
	err = a.db.View(func(txn *bolt.Tx) (err error) {
		apiKey, err = a.get(txn, []byte(id))
		return
	})

	return
}

// GetByOwner will return the APIKeys associated with the provided owner id
func (a *APIKeys) GetByOwner(ownerID string) (apiKeys []*APIKey, err error) {
	err = a.db.View(func(txn *bolt.Tx) (err error) {
		var ids [][]byte
		if ids, err = a.getOwnedIDs(txn, []byte(ownerID)); err != nil {
			return
		}

		apiKeys, err = a.getSlice(txn, ids)
		return
	})

	return
}

// Put will edit an APIKey's label
func (a *APIKeys) Put(key, label string) (err error) {
	keyBytes := []byte(key)
	err = a.db.Update(func(txn *bolt.Tx) (err error) {
		var apiKey *APIKey
		if apiKey, err = a.get(txn, keyBytes); err != nil {
			return
		}

		apiKey.Label = label
		return a.put(txn, keyBytes, apiKey)
	})

	return
}

// Delete will remove a apiKey
func (a *APIKeys) Delete(key string) (err error) {
	err = a.db.Update(func(txn *bolt.Tx) (err error) {
		return a.delete(txn, []byte(key))
	})

	return
}

// Close will close the apiKeys service
func (a *APIKeys) Close() (err error) {
	return a.db.Close()
}
